package ru.dimon.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class MoneyTest {

    @Test
    void testMultiplication() {
        Money fiveDollar = Money.dollar(5);
        assertEquals(Money.dollar(20), fiveDollar.times(4));
        assertEquals(Money.dollar(15), fiveDollar.times(3));

        Money fiveRouble = Money.rouble(5);
        assertEquals(Money.rouble(10), fiveRouble.times(2));
    }

    @Test
    void testEquality() {
        assertEquals(Money.dollar(5), Money.dollar(5));
        assertEquals(Money.rouble(7), Money.rouble(7));
        assertNotEquals(Money.dollar(5), Money.dollar(7));
        assertNotEquals(Money.dollar(6), Money.rouble(6));
    }

    @Test
    void testCurrency() {
        assertEquals("USD", Money.dollar(1).currency());
        assertEquals("RUB", Money.rouble(1).currency());
    }

    @Test
    void testSimpleAddition() {
        Money fiveDollar = Money.dollar(5);
        Expression sum = fiveDollar.plus(fiveDollar);
        Bank bank = new Bank();
        Money reduced = bank.reduce(sum, "USD");
        assertEquals(Money.dollar(10), reduced);
    }

    @Test
    void testReduceSum() {
        Expression sum = new Sum(Money.dollar(3), Money.dollar(4));
        Bank bank = new Bank();
        Money result = bank.reduce(sum, "USD");
        assertEquals(Money.dollar(7), result);
    }

    @Test
    void testReduceMoney() {
        Bank bank = new Bank();
        Money result = bank.reduce(Money.dollar(1), "USD");
        assertEquals(Money.dollar(1), result);
    }

    @Test
    void testPlusReturnSum() {
        Money fiveDollar = Money.dollar(5);
        Expression result = fiveDollar.plus(fiveDollar);
        Sum sum = (Sum) result;
        assertEquals(fiveDollar, sum.augmend);
        assertEquals(fiveDollar, sum.addmend);
    }

    @Test
    void testReduceMoneyDifferenceCurrency() {
        Bank bank = new Bank();
        bank.addRate("RUB", "USD", 80);
        Money result = bank.reduce(Money.rouble(80), "USD");
        assertEquals(Money.dollar(1), result);
    }

    @Test
    void testIdentityRate() {
        assertEquals(1, new Bank().rate("USD", "USD"));
        assertEquals(1, new Bank().rate("RUB", "RUB"));
    }

    @Test
    void testMixedAdditional() {
        Expression fiveDollars = Money.dollar(5);
        Expression fourHundredRoubles = Money.rouble(400);
        Bank bank = new Bank();
        bank.addRate("RUB", "USD", 80);
        Money result = bank.reduce(fiveDollars.plus(fourHundredRoubles), "USD");
        assertEquals(Money.dollar(10), result);
    }

    @Test
    void testSumPlusMoney() {
        Expression fiveDollars = Money.dollar(5);
        Expression fourHundredRoubles = Money.rouble(400);
        Bank bank = new Bank();
        bank.addRate("RUB", "USD", 80);
        Expression sum = new Sum(fiveDollars, fourHundredRoubles).plus(fiveDollars);
        Money result = bank.reduce(sum, "USD");
        assertEquals(Money.dollar(15),result);
    }

    @Test
    void testSumTimes() {
        Expression fiveDollars = Money.dollar(5);
        Expression fourHundredRoubles = Money.rouble(400);
        Bank bank = new Bank();
        bank.addRate("RUB", "USD", 80);
        Expression sum = new Sum(fiveDollars, fourHundredRoubles).times(2);
        Money result = bank.reduce(sum, "USD");
        assertEquals(Money.dollar(20), result);
    }
}
